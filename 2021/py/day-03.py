# Goals: 
# - Complete day 3 with no additional modules
# - Read lists as few times as possible 
# - Avoid copying data, only create references
# - This could possibly be done in signifcantly less code with numpy

def get_commons_col(digits, col, flag, n):

    if len(digits) == 1 or col == n:
        return digits

    temp0, temp1 = [], []
    val = 0
    for i, line in enumerate(digits):
        num = line[col]
        val += num

        # Create references to the lists in digits
        if num == 0:
            temp0.append(digits[i])
        elif num == 1:
            temp1.append(digits[i])

    mstc = 1 if val / (i + 1.0) >= 0.5 else 0

    mstcl = temp0 if mstc == 0 else temp1
    lstcl = temp1 if mstc == 0 else temp0

    if flag == 'mstc':
        return get_commons_col(mstcl, col + 1, flag, n)

    if flag == 'lstc':
        return get_commons_col(lstcl, col + 1, flag, n)

    if flag == 'initial':
        return mstcl, lstcl

def bintodec(digits):
    nb = len(digits) - 1
    return sum(num * 2**(nb - i) for i, num in enumerate(digits))

def part_one():
    with open('../data/three.txt') as f:
        digits = [int(num) for num in 
                  list(f.readline().strip())]

        for i, line in enumerate(f):
            line = list(line.strip())
            digits = [val + int(num) for val, num in 
                      zip(digits, line)]

    gamma2 = [1 if num/(i + 1.0) >= 0.5 else 0 for num in digits]
    epsil2 = [0 if num == 1 else 1 for num in gamma2]
    
    return bintodec(gamma2)*bintodec(epsil2)


def part_two():
    with open('../data/three.txt') as f:
        digits = [[int(num) for num in list(line.strip())]
                   for line in f]

    n = len(digits[0])

    # Initial division into oxygen generator and co2 scrubber
    oxygen, co2scr = get_commons_col(digits, 0, 'initial', n)

    # Apply filters
    oxygen = get_commons_col(oxygen, 1, 'mstc', n)
    co2scr = get_commons_col(co2scr, 1, 'lstc', n)

    return bintodec(oxygen[0])*bintodec(co2scr[0])

if __name__ == '__main__':
    result = part_one()
    print('Part 1:', result)

    result = part_two()
    print('Part 2:', result)
def read_board(f):
    board = []
    for i, line in zip(range(5), f):
        board += map(int, line.strip().split())
    return board

def board_wins(board):
    return True if any(sum(board[i*5:(i + 1)*5]) == -5 
                       or sum(board[i::5]) == -5 
                       for i in range(5)) else False

def parts_one_two():
    with open('../data/four.txt') as f:

        # Read numbers to draw and advance blank line
        numbers = [int(num) for num in f.readline().split(',')]
        f.readline()

        # Read boards
        boards = []
        while True:
            boards.append(read_board(f))
            try:
                next(f)
            except StopIteration:
                break

    # Read first five numbers
    nb = len(boards)
    winners = []
    for j, num in enumerate(numbers):
        for m, board in enumerate(boards):
            if m in winners: continue
            try:
                board[board.index(num)] = -1
            except ValueError:
                continue

            if j > 5 and board_wins(board):
                winners.append(m)
                if len(winners) == 1:
                    result1 = sum([val for val in board if val != -1])*num
                elif len(winners) == nb:
                    return result1, sum([val for val in board if val != -1])*num


if __name__ == '__main__':
    result1, result2 = parts_one_two()
    print('Part 1:', result1)
    print('Part 2:', result2)
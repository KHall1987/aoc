def part_one(course):
    h, d = 0, 0
    for command, value in course:
        value = int(value)
        if command == 'forward':
            h += value
        elif command == 'down':
            d += value
        elif command == 'up':
            d -= value
    return d*h 

def part_two(course):
    h, d, aim = 0, 0, 0
    for command, value in course:
        value = int(value)
        if command == 'down':
            aim += value
        elif command == 'up':
            aim -= value
        elif command == 'forward':
            h += value
            d += aim*value
    return d*h

if __name__ == '__main__':
    with open('../data/two.txt') as f:
        data = [line.split() for line in f]

    result = part_one(data)
    print('Part 1:', result)

    result = part_two(data)
    print('Part 2:', result)
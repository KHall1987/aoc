# AoC 2021

Solutions to Advent of Code 2021 using Python and C++ for now. For Python, the goal is to write code without importing any additional libraries.
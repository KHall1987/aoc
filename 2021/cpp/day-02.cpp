#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

int main(int argc, char const *argv[])
{
  char command[40];
  int value,h,d; 

  FILE *fid = fopen("../data/two.txt", "r");

  // Part one
  h = 0;
  d = 0;
  while(!feof(fid))
  {
    fscanf(fid,"%s %d",command,&value);
    if(!strcmp(command,"forward")) h += value;
    if(!strcmp(command,"down"))    d += value;
    if(!strcmp(command,"up"))      d -= value;
  }
  fclose(fid);
  printf("Part 1: %d\n", h*d);

  // Part 2
  fid = fopen("../data/two.txt", "r");

  int aim;
  h = 0;
  d = 0;
  aim = 0;
  while(!feof(fid))
  {
    fscanf(fid,"%s %d",command,&value);
    if(!strcmp(command,"forward"))
    { 
      h += value;
      d += aim*value;
    }
    if(!strcmp(command,"down"))    aim += value;
    if(!strcmp(command,"up"))      aim -= value;
  }
  fclose(fid);
  printf("Part 2: %d\n", h*d);
  return 0;
}

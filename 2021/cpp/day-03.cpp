#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cmath>

using namespace std;

int bin_to_dec(int *digits, int n)
{
  int num = 0;
  for(int i=0; i<n; i++)
  {
    num += digits[i]*pow(2,n-1-i);
  }
  return num;
}

int main(int argc, char const *argv[])
{
  char binstr[20];
  int n = 12; // Number of digits per row
  int digits[n],c,num,gam,eps;
  float temp;

  // Part one
  for(int i=0; i<n; i++)  digits[i] = 0;

  FILE *fid = fopen("../data/three.txt", "r");
  c = 0;
  while(!feof(fid))
  {
    fscanf(fid,"%s",binstr);
    for(int i=0; i<n; i++)
    {
      num = binstr[i] - 48; 
      if(num == 1) digits[i]++;
    }
    c++;
  }
  fclose(fid);

  // Compute the gamma rate
  for(int i=0; i<n; i++)
  {
    temp = (float) digits[i] / (float) c ;
    if(temp > 0.5) digits[i] = 1;
    else digits[i] = 0;
  }
  gam = bin_to_dec(digits,n);

  // Compute the epsilon rate
  for(int i=0; i<n; i++) digits[i] = abs(digits[i] - 1);
  eps = bin_to_dec(digits,n);
  
  printf("Part 1: %d\n", gam*eps);
  return 0;
}

#include <iostream>
#include <fstream>
#include <vector>

int main(int argc, char const *argv[])
{ 
  // --- Part 1 ---
  std::fstream file("../data/one.txt");
  int val1,val2,counter; 

  val1 = 100000000;
  while(file >> val2)
  { 
    if(val2 > val1) counter++; 
    val1 = val2;
  }
  file.close();
  printf("Part 1: %d\n", counter);

  // -- Part 2 ---
  file.open("../data/one.txt");
  std::vector<int> array;

  while(file >> val1)
  {
    array.push_back(val1);
  }
  file.close();

  // Determine if b+c+d>a+b+c => d>a
  counter = 0;
  for(int i=0; i<array.size(); i++)
  {
    if(array[i+2] > array[i]) counter++;
  }
  printf("Part 2: %d\n", counter);

  return 0;
}